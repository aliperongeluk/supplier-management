const express = require('express');
const routes = express.Router();

const Supplier = require('../models/supplier.model');

/**
 * @swagger
 * /api/supplier-management/suppliers/:
 *    get:
 *     tags:
 *       - suppliers
 *     description: Retrieving all suppliers.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Suppliers are returned.
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', (req, res) => {
  Supplier.find({})
    .then(p => {
      res.status(200).json(p);
    })
    .catch(error => {
      console.log({ error: error });
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/supplier-management/suppliers/{id}/:
 *    get:
 *     tags:
 *       - suppliers
 *     description: Retrieving supplier with given ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *         type: string
 *         required: true
 *         description: String ID of the product.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Suppliers are returned.
 *       400:
 *          description: Something went wrong.
 */
routes.get('/:id', (req, res) => {
  const id = req.params.id;

  Supplier.findById(id)
    .then(p => res.status(200).json(p))
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/supplier-management/suppliers/:
 *    post:
 *     tags:
 *       - suppliers
 *     description: Creating new supplier in supplier database. Also adds the newly created supplier to the message broker.
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Create new supplier object.
 *         schema:
 *             $ref: '#/definitions/Supplier'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Supplier is saved in database and returned.
 *       400:
 *          description: Something went wrong.
 */
routes.post('/', (req, res) => {
  const p = new Supplier(req.body);

  p.save()
    .then(() => {
      res.status(200).send(p);
    })
    .catch(error => {
      console.log({ error: error });
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/supplier-management/suppliers/{id}/:
 *    delete:
 *     tags:
 *       - suppliers
 *     description: Delete supplier with given ID from database.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *         type: string
 *         required: true
 *         description: String ID of the supplier.
 *     responses:
 *       200:
 *          description: Supplier is deleted and returned.
 *       400:
 *          description: Something went wrong.
 */
routes.delete('/:id', (req, res) => {
  const id = req.params.id;

  Supplier.findByIdAndDelete(id)
    .then(p => res.status(200).send(p))
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

module.exports = routes;
