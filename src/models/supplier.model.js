const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @swagger
 * definitions:
 *  Supplier:
 *    type: object
 *    properties:
 *      name:
 *        type: string
 *      address:
 *        type: string
 *      city:
 *        type: string
 *      type:
 *        type: string
 *        enum:
 *          - REGULAR
 *          - TRUSTED
 *      postalCode:
 *        type: string
 */
const SupplierSchema = new Schema({
  name: {
    type: String,
    required: [true, 'name is required.'],
  },
  address: {
    type: String,
    required: [true, 'address is required.'],
  },
  city: {
    type: String,
    required: [true, 'city is required.'],
  },
  type: {
    type: String,
    enum: ['REGULAR', 'TRUSTED'],
    required: [true, 'type is required.'],
  },
  postalCode: {
    type: String,
    required: [true, 'postalCode is required.'],
  },
});

const Supplier = mongoose.model('Supplier', SupplierSchema);

module.exports = Supplier;
