const swaggerJsDoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      title: 'Supplier Management',
      version: '1.0.0',
      description:
        'Swagger documentation for Supplier Management microservice (AliPerOngeluk).',
    },
  },

  apis: ['./src/routes/*.js', './src/models/*.js'],
};

const specs = swaggerJsDoc(options);

module.exports = specs;
