const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/config/swagger');
const mongo = require('./src/connections/mongodb.connection');

const express = require('express');
const app = express();
const port = process.env.PORT || 7002;

const supplierRoutes = require('./src/routes/supplier.routes');

const bodyParser = require('body-parser');

const { eurekaClient } = require('./src/environment/config/eureka.config');

app.use(bodyParser.json());

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/suppliers', supplierRoutes);

// default:
app.use('*', (req, res) => {
  res.status(400).send({
    error: 'not available',
  });
});

if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line no-console
  console.log('Starting eureka connection...');
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});
